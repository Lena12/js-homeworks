/* 
Теоретический вопрос
Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
Это иерархия  тегов, текста, в общем, всего содержимого страницы, явлющегося объектами, которые можно изменять.
*/

let cities = [
    'Chernigiv',
    'Kiev',
    'Kharkiv',
    'Odessa',
    'Lviv'
]

function showCities(cities) {
    let citiesLi = cities.map(function (city) {
        return `<li>${city}</li>`;

    });

    let citiesOnPage = citiesLi.join("");
    let citiesOnPageUl = `<ul>${citiesOnPage}</ul>`;
    console.log(citiesOnPageUl);

    let citiesInDiv = document.getElementById("app");
    citiesInDiv.innerHTML = citiesOnPageUl;
}
showCities(cities);