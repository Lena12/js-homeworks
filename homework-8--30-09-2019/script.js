const field = document.getElementById("input-id");
const body = document.getElementsByTagName("body")[0];
const container = document.getElementById("before-container");

field.addEventListener("focus", function() {
  field.classList.add("active");
  const mistakeSpan = document.getElementById("mistake-span");
  const fieldValue = document.getElementById("field-value");
  if (fieldValue) {
    fieldValue.remove();
  }

  if (mistakeSpan) {
    mistakeSpan.remove();
  }
});

field.addEventListener("blur", function() {
  field.classList.remove("active");
  if (!field.value || +field.value <= 0 || isNaN(field.value)) {
      field.classList.add("mistake-field");
      const mistakeSpan =
      document.getElementById("mistake-span") || document.createElement("p");
      mistakeSpan.id = "mistake-span";
      mistakeSpan.innerText = "Please enter correct price";
      field.after(mistakeSpan);
  } else if (+field.value >= 1) {
      const mistakeSpan = document.getElementById("mistake-span");
      const fieldValue = document.createElement("span");
      fieldValue.className = "field-value";
      fieldValue.innerText = `Текущая цена: ${field.value}`;
      const btnClose = document.createElement("span");
      btnClose.innerText = "X";
      btnClose.onclick = function() {
      this.parentNode.remove();
      field.value = "";
    };
    fieldValue.appendChild(btnClose);
    btnClose.className = "btn-close";
    container.appendChild(fieldValue);
    field.classList.add("right");
    field.classList.remove("mistake-field");

    if (mistakeSpan) {
      mistakeSpan.innerHTML = "";
    }
  }
});
