//Теоретический вопрос
//Почему для работы с input не рекомендуется использовать события клавиатуры?
//Ввести информацию можно не только с помощью кнопок, можно и вставить что-то или использовать распознавание речи. Поэтому для отслеживания ввода в поле <input> нужно не просто клавиатурное событие, а событие input, которое может отследить любые изменения.
//

const buttons = document.querySelectorAll(".btn-wrapper .btn");
const enter = document.getElementById("enter");
const keyS = document.getElementById("s");
const keyE = document.getElementById("e");
const keyO = document.getElementById("o");
const keyN = document.getElementById("n");
const keyL = document.getElementById("l");
const keyZ = document.getElementById("z");

document.addEventListener('keydown', function(event) {
    if (event.code == 'KeyZ') {
        clearActiveClassBtn();
        keyZ.classList.add("active1");
    }
    if (event.code == 'KeyL') {
        clearActiveClassBtn();
        keyL.classList.add("active1");
    }
    if (event.code == 'KeyN') {
        clearActiveClassBtn();
        keyN.classList.add("active1");
    }
    if (event.code == 'KeyO') {
        clearActiveClassBtn();
        keyO.classList.add("active1");
    }
    if (event.code == 'KeyE') {
        clearActiveClassBtn();
        keyE.classList.add("active1");
    }
    if (event.code == 'KeyS') {
        clearActiveClassBtn();
        keyS.classList.add("active1");
    }
    if (event.code == 'Enter') {
        clearActiveClassBtn();
        enter.classList.add("active1");
    }
});

function clearActiveClassBtn () {
    buttons.forEach(function (item) {
        item.classList.remove("active1");
    });
}




